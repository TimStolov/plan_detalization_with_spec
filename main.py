import csv

from model.entities import Entities
from model.plan import Plan


def transform_plan(plan_csv, spec_csv, report_csv):
    entities = Entities()
    with open(spec_csv, newline='', encoding='UTF-8') as csv_file:
        spec_data = csv.DictReader(csv_file)
        for row in spec_data:
            parent = entities.add_entity(
                code=row['PARENT_CODE'],
                identity=row['PARENT_IDENTITY'],
                name=row['PARENT_NAME']
            )
            child = entities.add_entity(
                code=row['CODE'],
                identity=row['IDENTITY'],
                name=row['NAME']
            )
            parent.add_child(child, float(row['AMOUNT']))

    plan = Plan()
    with open(plan_csv, newline='', encoding='UTF-8') as csv_file:
        plan_data = csv.DictReader(csv_file)
        for row in plan_data:
            plan.add_entry(
                name=row['ORDER'],
                date_from=row['DATE'],
                date_to=row['DATE'],
                entity=entities.entities[row['CODE']],
                amount=float(row['AMOUNT'])
            )

    answer = plan.explode_plan()

    with open(report_csv, 'w', newline='') as csv_file:
        report = csv.DictWriter(csv_file, answer[0].keys())
        report.writeheader()
        report.writerows(answer)


if __name__=='__main__':
    transform_plan(
        'example/plan_new.csv',
        'example/specification.csv',
        'example/report_plan.csv'
    )
