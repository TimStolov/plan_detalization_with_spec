from collections import defaultdict


class Entity(object):
    def __init__(self, code, identity, name):
        self.code = code
        self.identity = identity
        self.name = name
        self.spec = defaultdict(float)

    def add_child(self, entity, amount):
        self.spec[entity] += amount

    def explode_entity(self, amount, report=None):
        if report is None:
            report = {
                self: amount
            }
        for item in self.spec:
            if item in report:
                report[item] += amount * self.spec[item]
            else:
                report[item] = amount * self.spec[item]
            report = item.explode_entity(amount * self.spec[item], report)
        return report
