from model.order import Order
from model.plan_entry import PlanEntry


class Plan(object):
    def __init__(self):
        self.entries = []
        self.orders = {}

    def add_order(self, name, date_from, date_to):
        if name not in self.orders:
            self.orders[name] = Order(name, date_from, date_to)
        return self.orders[name]

    def add_entry(self, entity, amount, name, date_from, date_to):
        self.entries.append(
            PlanEntry(
                order=self.add_order(name, date_from, date_to),
                entity=entity,
                amount=amount
            )
        )

    def explode_plan(self):
        report = []
        for entry in self.entries:
            for item, amount in entry.entity.explode_entity(entry.amount).items():
                report.append({
                    'ORDER': entry.order.name,
                    'CODE': item.code,
                    'NAME': item.name,
                    'IDENTITY': item.identity,
                    'DATE': entry.order.date_from,
                    'AMOUNT': amount
                })
        return report
