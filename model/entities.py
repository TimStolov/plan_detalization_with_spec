from model.entity import Entity


class Entities(object):
    def __init__(self):
        self.entities = {}

    def add_entity(self, code, identity, name):
        if code not in self.entities:
            self.entities[code] = Entity(code, identity, name)

        return self.entities[code]